package com.example.staggerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.staggerview.databinding.ItemDoubleBinding

class DoubleAdapter : RecyclerView.Adapter<DoubleAdapter.DoubleViewHolder>() {

    private var doubles = listOf<Double>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoubleViewHolder {
        val binding = ItemDoubleBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return DoubleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DoubleViewHolder, position: Int) {
        val double = doubles[position]
        holder.displayDouble(double)
    }

    override fun getItemCount(): Int {
        return doubles.size
    }

    fun addDoubles(d: List<Double>) {
        this.doubles = d.toMutableList()
        notifyDataSetChanged()
    }

    class DoubleViewHolder(
        private val binding: ItemDoubleBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun displayDouble(d: Double) {
            binding.tvItemDouble.text = d.toString()
        }
    }
}