package com.example.staggerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.staggerview.databinding.ItemFirstBinding
import com.example.staggerview.view.FirstFragmentDirections

class FirstAdapter : RecyclerView.Adapter<FirstAdapter.FirstViewHolder>() {
    private var categories = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FirstViewHolder {
        val binding = ItemFirstBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return FirstViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FirstViewHolder, position: Int) {
        val category = categories[position]
        holder.bindCategory(category)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun addCategories(name: List<String>) {
        this.categories = name.toMutableList()
        notifyDataSetChanged()
    }

    class FirstViewHolder(
        private val binding: ItemFirstBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindCategory(name: String) {
            binding.tvCategory.text = name

            when (name) {
                "Strings" -> {
                    binding.tvCategory.setOnClickListener {
                        it.findNavController()
                            .navigate(FirstFragmentDirections.actionFirstFragmentToStringFragment())
                    }
                }
                "Integer" -> {
                    binding.tvCategory.setOnClickListener {
                        it.findNavController()
                            .navigate(FirstFragmentDirections.actionFirstFragmentToFragmentInteger())
                    }
                }
                "Double" -> {
                    binding.tvCategory.setOnClickListener {
                        it.findNavController()
                            .navigate(FirstFragmentDirections.actionFirstFragmentToDoubleFragment())
                    }
                }
            }
        }
    }
}
