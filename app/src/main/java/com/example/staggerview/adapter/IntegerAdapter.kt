package com.example.staggerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.staggerview.databinding.ItemIntegerBinding

class IntegerAdapter : RecyclerView.Adapter<IntegerAdapter.IntegerViewHolder>() {
    private var integers = listOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntegerViewHolder {
        val binding = ItemIntegerBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return IntegerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: IntegerViewHolder, position: Int) {
        val integer = integers[position]
        holder.displayInteger(integer)
    }

    override fun getItemCount(): Int {
        return integers.size
    }

    fun addIntegers(int: List<Int>) {
        this.integers = int.toMutableList()
        notifyDataSetChanged()
    }

    class IntegerViewHolder(
        private val binding: ItemIntegerBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun displayInteger(int: Int) {
            binding.tvItemInteger.text = int.toString()
        }
    }
}