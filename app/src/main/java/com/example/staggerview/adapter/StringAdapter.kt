package com.example.staggerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.staggerview.databinding.ItemStringBinding

class StringAdapter : RecyclerView.Adapter<StringAdapter.StringViewHolder>() {
    private var strings = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringViewHolder {
        val binding = ItemStringBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return StringViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        val string = strings[position]
        holder.displayString(string)
    }

    override fun getItemCount(): Int {
        return strings.size
    }

    fun addStrings(string: List<String>) {
        this.strings = string.toMutableList()
        notifyDataSetChanged()
    }

    class StringViewHolder(
        private val binding: ItemStringBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun displayString(string: String) {
            binding.tvItemString.text = string
        }
    }
}