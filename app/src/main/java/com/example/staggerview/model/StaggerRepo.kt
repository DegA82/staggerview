package com.example.staggerview.model

object StaggerRepo : StaggerService {

    override suspend fun getCategory(): List<String> {
        return listOf("Strings", "Integer", "Double")
    }

    override suspend fun getStrings(): List<String> {
        return listOf(
            "Kotlin",
            "Java",
            "Ruby",
            "CSS",
            "Recycler View",
            "Linear Layout",
            "Context",
            "Python",
            "XML",
            "Binding"
        )
    }

    override suspend fun getInteger(): List<Int> {
        return listOf(1, 4, 3, 5, 6, 33, 44, 55, 66, 49, 43, 23, 34)
    }

    override suspend fun getDoubles(): List<Double> {
        return listOf(1.22, 3.22, 44.5, 22.2, 33.3, 44.4, 55.5, 66.6, 77.7, 88.8)
    }
}