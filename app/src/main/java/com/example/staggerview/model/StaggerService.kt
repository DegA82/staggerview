package com.example.staggerview.model

interface StaggerService {

    suspend fun getCategory(): List<String>
    suspend fun getStrings(): List<String>
    suspend fun getInteger(): List<Int>
    suspend fun getDoubles(): List<Double>
}