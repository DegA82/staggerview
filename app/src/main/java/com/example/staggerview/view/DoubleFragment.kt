package com.example.staggerview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.staggerview.R
import com.example.staggerview.adapter.DoubleAdapter
import com.example.staggerview.databinding.FragmentDoubleBinding
import com.example.staggerview.viewmodel.StaggerVieweModel

class DoubleFragment : Fragment(R.layout.fragment_double) {

    private var _binding: FragmentDoubleBinding? = null
    private val binding get() = _binding!!
    private val staggerVieweModel by viewModels<StaggerVieweModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDoubleBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doubleView()
    }

    private fun doubleView() {
        with(binding.rvDouble) {
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
            adapter = DoubleAdapter().apply {
                with(staggerVieweModel) {
                    getDouble()
                    doubleList.observe(viewLifecycleOwner) {
                        addDoubles(it)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}