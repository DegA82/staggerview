package com.example.staggerview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.staggerview.adapter.FirstAdapter
import com.example.staggerview.databinding.FragmentFirstBinding
import com.example.staggerview.viewmodel.StaggerVieweModel

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private val staggerVieweModel by viewModels<StaggerVieweModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFirstBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFirstFragmentView()
    }

    private fun initFirstFragmentView() {
        with(binding.rvFirstPage) {
            layoutManager =
                StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL)
            adapter = FirstAdapter().apply {
                with(staggerVieweModel) {
                    getCategory()
                    category.observe(viewLifecycleOwner) {
                        addCategories(it)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}