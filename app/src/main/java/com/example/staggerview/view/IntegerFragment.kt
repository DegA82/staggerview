package com.example.staggerview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.staggerview.adapter.IntegerAdapter
import com.example.staggerview.databinding.FragmentIntegerBinding
import com.example.staggerview.viewmodel.StaggerVieweModel

class IntegerFragment : Fragment() {

    private var _binding: FragmentIntegerBinding? = null
    private val binding get() = _binding!!
    private val staggerVieweModel by viewModels<StaggerVieweModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentIntegerBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        with(binding.rvInteger) {
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
            adapter = IntegerAdapter().apply {
                with(staggerVieweModel) {
                    getInteger()
                    intList.observe(viewLifecycleOwner) {
                        addIntegers(it)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}