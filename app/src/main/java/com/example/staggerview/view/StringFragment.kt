package com.example.staggerview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.staggerview.adapter.StringAdapter
import com.example.staggerview.databinding.FragmentStringBinding
import com.example.staggerview.viewmodel.StaggerVieweModel

class StringFragment : Fragment() {

    private var _binding: FragmentStringBinding? = null
    private val binding get() = _binding!!
    private val staggerViewModel by viewModels<StaggerVieweModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentStringBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        stringView()
    }

    private fun stringView() {
        with(binding.rvString) {
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
            adapter = StringAdapter().apply {
                with(staggerViewModel) {
                    getStrings()
                    stringList.observe(viewLifecycleOwner) {
                        addStrings(it)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}