package com.example.staggerview.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.staggerview.model.StaggerRepo
import kotlinx.coroutines.launch

class StaggerVieweModel : ViewModel() {

    private val repo = StaggerRepo

    private val _category = MutableLiveData<List<String>>()
    val category: LiveData<List<String>> get() = _category

    private val _stringList = MutableLiveData<List<String>>()
    val stringList: LiveData<List<String>> get() = _stringList

    private val _intList = MutableLiveData<List<Int>>()
    val intList: LiveData<List<Int>> get() = _intList

    private val _doubleList = MutableLiveData<List<Double>>()
    val doubleList: LiveData<List<Double>> get() = _doubleList

    fun getCategory() {
        viewModelScope.launch {
            val category = repo.getCategory()
            _category.value = category
        }
    }

    fun getStrings() {
        viewModelScope.launch {
            val stringList = repo.getStrings()
            _stringList.value = stringList
        }
    }

    fun getInteger() {
        viewModelScope.launch {
            val intList = repo.getInteger()
            _intList.value = intList
        }
    }

    fun getDouble() {
        viewModelScope.launch {
            val doubleList = repo.getDoubles()
            _doubleList.value = doubleList
        }
    }
}